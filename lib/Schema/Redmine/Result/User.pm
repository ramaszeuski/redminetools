package Schema::Redmine::Result::User;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('users');
__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    qw(
        login
        hashed_password
        firstname
        lastname
        admin
        status
        last_login_on
        language
        auth_source_id
        created_on
        updated_on
        type
        identity_url
        mail_notification
        salt
        must_change_passwd
        passwd_changed_on
        sso_u_id
    )
);

__PACKAGE__->set_primary_key('id');


1;


