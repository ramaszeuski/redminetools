package Schema::Redmine::Result::MemberRole;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('member_roles');
__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    qw(
        member_id
        role_id
        inherited_from
    )
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    member => 'Schema::Redmine::Result::Member',
    { 'foreign.id' => 'self.member_id', },
);

1;
