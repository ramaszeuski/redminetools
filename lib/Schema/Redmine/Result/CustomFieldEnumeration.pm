package Schema::Redmine::Result::CustomFieldEnumeration;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('custom_field_enumerations');
__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    qw(
        id
        custom_field_id
        name
        active
        position
    )
);

__PACKAGE__->set_primary_key('id');

1;

