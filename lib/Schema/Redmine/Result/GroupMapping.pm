package Schema::Redmine::Result::GroupMapping;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('group_mappings');
__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    qw(
        variable
        group_id
    )
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    group_users => 'Schema::Redmine::Result::GroupUser',
    { 'foreign.group_id' => 'self.group_id', },
);

1;
