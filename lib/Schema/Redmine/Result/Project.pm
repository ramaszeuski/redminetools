package Schema::Redmine::Result::Project;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('projects');
__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    qw(
        name
        description
        homepage
        is_public
        parent_id
        created_on
        updated_on
        identifier
        status
        lft
        rgt
        inherit_members
        default_version_id
        default_assigned_to_id
    )
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->might_have(
    members => 'Schema::Redmine::Result::Member',
    { 'foreign.project_id' => 'self.id', },
);

1;

