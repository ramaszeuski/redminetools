package Schema::Redmine::Result::CustomValue;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('custom_values');
__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    qw(
        id
        customized_type
        customized_id
        custom_field_id
        value
    )
);

__PACKAGE__->set_primary_key('id');

1;

