package Schema::Redmine::Result::Member;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('members');
__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    qw(
        user_id
        project_id
        created_on
        mail_notification
    )
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    'key2' => [qw(user_id project_id)]
);

__PACKAGE__->has_many(
    member_roles => 'Schema::Redmine::Result::MemberRole',
    { 'foreign.member_id' => 'self.id', },
);

1;
