package Schema::Redmine::Result::GroupUser;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('groups_users');
__PACKAGE__->add_columns(
    qw(
        group_id
        user_id
    )
);

__PACKAGE__->set_primary_key('group_id', 'user_id');

__PACKAGE__->belongs_to(
    user => 'Schema::Redmine::Result::User',
    { 'foreign.id' => 'self.user_id', },
);

__PACKAGE__->belongs_to(
    group => 'Schema::Redmine::Result::GroupMapping',
    { 'foreign.group_id' => 'self.group_id', },
);

1;

