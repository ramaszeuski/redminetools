package Schema::Redmine::Result::Contact;
use base qw/DBIx::Class::Core/;

__PACKAGE__->table('contacts');
__PACKAGE__->add_columns(
    id => {
        data_type => 'integer',
        is_auto_increment => 1,
    },
    qw(
        id
        first_name
        last_name
        middle_name
        company
        phone
        email
        website
        skype_name
        birthday
        avatar
        background
        job_title
        is_company
        author_id
        assigned_to_id
        created_on
        updated_on
        cached_tag_list
        visibility
    )
);

__PACKAGE__->set_primary_key('id');

1;
