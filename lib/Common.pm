package Common;

use strict;
use warnings;
use utf8;

use Getopt::Long::Descriptive;
use Log::Dispatch;
use Log::Dispatch::Syslog;
use Log::Dispatch::Screen;
use YAML::Any;
use Time::HiRes qw(usleep gettimeofday);
use POSIX qw(strftime);
use FindBin qw($Bin);

use Schema::Redmine;

use constant SYSLOG_IDENT      => 'redmine_hacks';
use constant DEFAULT_LOG_LEVEL => 'info';

use constant OPTIONS  => [
  [ 'log-screen:s',  'Loglevel pro obrazovku', { requried => 1, default => 'info' } ],
  [ 'log-syslog:s',  'Loglevel pro syslog',    { requried => 1, default => 'info' } ],
  [ 'help|h',        'Tato napověda' ],
];

BEGIN {
    require Exporter;
    our $VERSION = 1.00;
    our @ISA     = qw(Exporter);
    our @EXPORT  = qw(
        $opt
        $usage
        $db

        trace
        error
    );

    binmode STDOUT, ':encoding(UTF-8)';
}

END {
    trace ('Ending');
}

use vars qw(
    $opt
    $usage
    $db
    $logger
);

sub init {
    my %args = @_;

    my @options = @{ (OPTIONS) };

    if ( exists $args{options} && ref $args{options} ) {
        @options = ( @{ $args{options} }, @options );
    }

    # parametry prikazove radky
    ( $opt, $usage ) = describe_options( '%c %o', @options );
    $usage->die() if $opt->help;

    # inicializace logovani
    $logger = Log::Dispatch->new();

    if ( $opt->log_screen =~ /(\w+)(,(\w+))?/ ) {

        my ( $min, $max ) = ( $1, $3 // 'emergency' );

        if ( $logger->level_is_valid($min) && $logger->level_is_valid($max) ) {
            $logger->add(
                Log::Dispatch::Screen->new(
                    name      => 'screen',
                    min_level => $min,
                    max_level => $max,
                    newline   => 1,
                    stderr    => 0,
                )
            );
        }
    }

    if ( $opt->log_syslog =~ /(\w+)(,(\w+))?/ ) {

        my ( $min, $max ) = ( $1, $3 // 'emergency' );

        if ( $logger->level_is_valid($min) && $logger->level_is_valid($max) ) {
            $logger->add(
                Log::Dispatch::Syslog->new(
                    name      => 'syslog',
                    min_level => $min,
                    max_level => $max,
                    ident     => $args{syslog_ident} // SYSLOG_IDENT,
                )
            );
        }
    }

    trace('Starting');

    $db = Schema::Redmine->connect(
        $ENV{DB_DSN},
        $ENV{DB_USERNAME},
        $ENV{DB_PASSWORD},
        {
            AutoCommit     => 1,
            quote_char     => '"',
            name_sep       => '.',
            pg_enable_utf8 => 1,
            on_connect_do  => [
                "set timezone to 'Europe/Prague'",
            ],
        }
    );
}

sub trace {
    my ( $message, $level, $args ) = @_;

    # na zacatku. kvuli minimailzaci odchylky
    my ($s, $us) = gettimeofday();

    if ( ! ( $level && $logger->level_is_valid($level)) ) {
        $level = DEFAULT_LOG_LEVEL;
    }

    return if ! $logger->would_log( $level );

    OUTPUT:
    foreach my $output ( keys %{ $logger->{outputs} } ) {

        my $prefix = "$0";
        $prefix =~ s/^\W+//;

        if ( $output =~ /^(file|screen)/) {
            $prefix = sprintf ('%s.%06d %s',
                strftime('%Y-%m-%d %H:%M:%S', localtime ),
                $us,
                $prefix
            );
        }

        my @msg = ();

        if ( ref $message eq 'ARRAY') {
            $message = join "|", @{ $message };
        }

       if ( $output eq 'syslog' ) {
            $message =~ s/\r/^M/g;
            @msg = split "\n", $message;
        }
        else {
            @msg = ( $message );
        }

        MSG:
        foreach my $msg ( @msg ) {
            $logger->log_to(
                name    => $output,
                level   => $level,
                message => "$prefix> $msg",
            )
        }
    }
}

1;
