FROM debian:bullseye-slim

RUN \
 dpkg --add-architecture i386 && \
 apt-get update && apt-get install -y \
 cpanminus \
 liblog-dispatch-perl \
 libdbix-class-perl \
 libdbd-pg-perl \
 libgetopt-long-descriptive-perl

RUN \
 cpanm \
 YAML::Any

COPY . /opt/redmine/

WORKDIR /opt/redmine
